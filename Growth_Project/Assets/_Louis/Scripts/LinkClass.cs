﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LinkClass : MonoBehaviour
{
    public float angle;
    public CelulleMain outputCell, inputCell;

    public void Init(CelulleMain OutputCell)
    {
        LineRenderer line = GetComponent<LineRenderer>();

        outputCell = OutputCell;
        Vector3 dir = (line.GetPosition(1) - OutputCell.transform.position).normalized;
        if (line.GetPosition(1).x <= OutputCell.transform.position.x)
        {
            angle = 360 - Vector3.Angle(outputCell.transform.forward, dir);
        }
        else if (line.GetPosition(1).x > OutputCell.transform.position.x)
        {
            angle = Vector3.Angle(outputCell.transform.forward, dir);
        }
        //angle = 180 -( 180 * Vector3.Dot(OutputCell.transform.forward, dir) );
        //Vector3 relative = outputCell.transform.InverseTransformDirection(line.GetPosition(1));
        //angle = Mathf.Atan2(relative.x, relative.z) * Mathf.Rad2Deg;
    }

    public void Transmitt()
    {
        outputCell.RemoveBlob(1);
        inputCell.AddBlob(1); 
        // on pourra lancer une anim ici 
    }

    public void Break()
    {
        outputCell.RemoveLink(this);
        inputCell.RemoveLink(this);
        gameObject.SetActive(false);
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class IsDragable : MonoBehaviour
{
    public bool hasBeenDropped;
    public bool isKingJelly;

    private Rigidbody rB;

    private void Awake()
    {
        rB = GetComponent<Rigidbody>();
        rB.constraints = RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        rB.useGravity = false;
    }

}

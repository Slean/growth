﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helper : MonoBehaviour
{
    public static int LoopIndex(int currentIndex, int maxIndex)
    {
        if (currentIndex >= maxIndex)
        {
            if (maxIndex <= 0 )
            {
                return 0;
            }
            currentIndex = currentIndex % maxIndex;
        }
        else if (currentIndex < 0)
        {
            currentIndex += maxIndex;
        }
        return currentIndex;
    }
    public static Vector3 RandomVectorInUpSphere()
    {
        Vector3 dir = Vector3.zero;
        return dir;
    }
    public static RaycastHit ReturnHit(Vector3 pos , Camera cam)
    {
        RaycastHit originhit;
        Ray ray = cam.ScreenPointToRay(pos);
        Physics.Raycast(ray, out originhit);
        return originhit;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CellSelection : MonoBehaviour
{
    public Button[] buttonTypes;
    [TextArea]
    public string Important;
    [Space(10f)]
    [Header("TWEAKING")]
    [Range(0f , 5f)]
    public float buttonDistance = 2f;

    [SerializeField]
    private RectTransform[] butTrans ; 

    private void Awake()
    {

        butTrans = new RectTransform[buttonTypes.Length];
        DesactiveButton();
        for (int i = 0; i < buttonTypes.Length; i++)
        {
            butTrans[i] = buttonTypes[i].GetComponent<RectTransform>();
        }
    }

    public void DesactiveButton()
    {
        foreach (Button button in buttonTypes)
        {
            button.gameObject.SetActive(false);
        }
    }

    public void ButtonPositions(CelulleMain inputCell)
    {
        GameObject[] select = inputCell.myCellTemplate.cellsEnableToBuild;
        // Attention division par zero pas possible sauf si j'ai Wif
        if (select.Length == 0)
        {
            Debug.Log("IL FAUT RAJOUTER LE CHECK POUR LE POSSIBILITE DE BUILD");
            return;
        }
        float anglefrac = 2*Mathf.PI / select.Length;
        for (int i = 0; i < select.Length; i++)
        {
            //calcule de l'angle en focntion du nombre depoitn
            float angle = anglefrac * i;
            Vector3 dir = new Vector3(Mathf.Sin(angle), Mathf.Cos(angle) , -1);
            Vector3 pos = dir * buttonDistance;
            StructureType actualType = select[i].GetComponent<CelulleMain>().myCellTemplate.type;
            ButtonChoosen(pos, actualType);
        }
    }

    private void ButtonChoosen(Vector3 pos , StructureType cellType)
    {
        Button currentButton = buttonTypes[(int)cellType];
        butTrans[(int)cellType].transform.localPosition = pos;
        currentButton.gameObject.SetActive(true);       
    }

    public void CellConstruction(GameObject cellToBuilt)
    {
        //faudra changer ça avec le pulling 
        GameObject newCell  = Instantiate(cellToBuilt, transform.position, Quaternion.identity);
        CellManager.cellManagerInstance.NewCellFromMenu(newCell , cellToBuilt.GetComponent<IsDragable>());     
    }
}

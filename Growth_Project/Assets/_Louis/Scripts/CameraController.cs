﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    #region Variables
    [Header("Movement")]
    [Space(5)]


    [Header("TWEAKING")]
    [Range(-10f, 10f)] public float moveSpeed = 10f; //speed actuelle de la cam
    public float accelerationAmount = 5f; //Taux d'accélération
    [Tooltip("en seconde")] public float accelerationTime = 2f; //Time to get to the max speed
    [Range(1f, 5f)] public float decelerationMultiplier = 4; //Taux de décélération


    [Header("Shifting")]
    private Vector3 cameraOrigin;
    public float shiftLength = 10;
    public float shiftSpeed = 1;

    // ratio accéleration par seconde; 
    private float accelerationRatio
    {
        get
        {
            return (accelerationAmount * Time.deltaTime) / accelerationTime;
        }
    }
    private float maxSpeed; //maxSpeed of the camera
    private float baseSpeed;

    [Header("Setup")]
    [Space(5)]
    [Range (20f , 50f)]
    public float startDistanceFromGround = 35f;

    #region Rotation
    /*
    [Header("Rotation")]
    [Space(5)]
    public float sensitivityX = 10f;
    public float sensitivityY = 10f;
    [Range(-360f, 360f)]
    public int minimumX, maximumX;
    [Range(-180f, 180f)]
    public int minimumY, maximumY;

    private float rotationY = 0f;
    */
    #endregion

    //REF
    public Transform groundTransform; 
    private Camera cam;



    //Boolean
    [Space(5)]
    [SerializeField]
    private bool inMovement = false;
    #endregion

    private void Awake()
    {
        transform.position = groundTransform.transform.position + new Vector3(0f, startDistanceFromGround, 0f);
        transform.LookAt(groundTransform);
        baseSpeed = moveSpeed;
        maxSpeed = moveSpeed + accelerationAmount;
        cam = Camera.main;
        cameraOrigin = transform.position;
    }



    private void Update()
    {

        #region Rotation //disable

        //rotation
        /*
        if (Input.GetMouseButton(1))
        {
            AdjustRotation();
        }
        */
        #endregion
        #region Deplacement

        GetInputs(out xDelta, out yDelta, out zDelta);
        if (xDelta != 0f || zDelta != 0f || yDelta != 0)
        {
            inMovement = true;
            AdjustPosition(xDelta, yDelta, zDelta);
        }
        else
        {
            inMovement = false;
            if (moveSpeed > baseSpeed)
            {
                moveSpeed -= accelerationRatio;
            }
        }

        CameraShift(CheckIfMouseInBorders());


        #endregion
    }

    float xDelta, yDelta, zDelta;
    Vector3 mousePos;
    private void GetInputs(out float xDelta, out float yDelta, out float zDelta)
    {
        mousePos = Input.mousePosition;
        xDelta = Input.GetAxis("Horizontal");
        yDelta = Input.GetAxis("Vertical");
        zDelta = Input.GetAxis("Mouse ScrollWheel");
        
        if (Input.GetAxis("Mouse ScrollWheel") == 0)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                zDelta = 1;
            }
            else if (Input.GetKey(KeyCode.LeftControl))
            {
                zDelta = -1;
            }
        }
        


    }

    Vector3 mouseDir;
    float camToOriginSqr;

    private void CameraShift(bool mouseIsInBorder)
    {
        mouseDir = new Vector3(mousePos.x - Screen.width / 2, 0, mousePos.y - Screen.height / 2).normalized;
        camToOriginSqr = (transform.position - cameraOrigin).sqrMagnitude;

        if (mouseIsInBorder)
        {
            if (camToOriginSqr < Mathf.Pow(shiftLength+.1f, 2))
            {
                transform.position += mouseDir * shiftSpeed * Time.deltaTime;
            }
            else
            {
                transform.position = cameraOrigin + mouseDir * shiftLength;
                Debug.Log("Jump");
            }
        }
        else
        {
            transform.position += (cameraOrigin - transform.position) * shiftSpeed * Time.deltaTime ; 
        }
    }

    private bool CheckIfMouseInBorders()
    {
        if(mousePos.x < Screen.width/5 || mousePos.x > Screen.width - Screen.width/5 || mousePos.y < Screen.height/5 || mousePos.y > Screen.height- Screen.height/5)
        {
            //Debug.Log("mouse in Border");
            return true;
        }
        else
        {
            //Debug.Log("mouse not in border");
            return false;
        }
    }

    private void AdjustPosition(float xDelta, float yDelta, float zDelta)
    {
        if (inMovement)
        {
            if (moveSpeed < maxSpeed)
            {
                moveSpeed += accelerationRatio;
            }
            if (moveSpeed >= maxSpeed)
            {
                moveSpeed = maxSpeed;
            }
        }
        Vector3 direction = transform.localRotation * new Vector3(xDelta, yDelta, zDelta).normalized;
        // forcement entre 0 et 1 , permet une décélération progressive
        float damping = Mathf.Clamp01(Mathf.Max(Mathf.Abs(xDelta), Mathf.Abs(yDelta), Mathf.Abs(zDelta)));
        float distance = moveSpeed * damping * Time.deltaTime;

        Vector3 position = cameraOrigin;
        position += direction * distance;
        //transform.localPosition = position;
        cameraOrigin = position;
    }

    //disable
    private void AdjustRotation()
    {
        /*
        float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;
        rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
        rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

        transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
        */
    }


}

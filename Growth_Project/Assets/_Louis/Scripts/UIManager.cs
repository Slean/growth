﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager uiManagerInstance;


    [Header("REF A FAIRE")]
    public CellSelection cellSelection;

    public bool inUI;


    private void Awake()
    {
        if (uiManagerInstance)
        {
            Destroy(gameObject);
        }
        else
        {
            uiManagerInstance = this;
            DontDestroyOnLoad(gameObject);
        }

        if (cellSelection == null)
        {
            Debug.LogError("Il faut rajouter , CellSelecetion sur Choose a Cell  ", cellSelection.transform);
        }
    }

    public void InUiSelection(Vector3 pos , CelulleMain originalCell , LineRenderer currentLine)
    {
        inUI = true;
        //c'est du debug
        currentLine.startColor = Color.red;
        currentLine.endColor = Color.red;
        cellSelection.transform.position = pos;
        cellSelection.gameObject.SetActive(true);
        cellSelection.ButtonPositions(originalCell);

    }

    public void CellCreationUI (Vector3 pos)
    {

    }

    public void DesactivateCellShop ()
    {
        inUI = false;
        cellSelection.DesactiveButton();
       // UI.SetActive(false);
    }


}
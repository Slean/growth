﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellManager : MonoBehaviour
{
    #region Variables
    public static CellManager cellManagerInstance; 

    //
    [Header("Tweaking")]
    public float yAddWhileDragging;
    public float delay;
    public float VerificationRadius;


    private bool creatingAnNewLink = false;
    private bool isDragging = false;
    private bool asAnObject;

    //REF;
    public GameObject linkPool;

    private IsDragable checkDrag;
    private Camera mainCamera;
    private Vector3 refSmooth;
    private Vector3 startPosLine, endPosLine;
    private CellSelection cellSelect;
    // Debug
    [Header("Debug")]
    [SerializeField]
    private GameObject currentObject;
    [SerializeField]
    private LineRenderer currentLine;
    [SerializeField]
    private Collider col;
    [SerializeField]
    private Rigidbody rg;
    [SerializeField]
    private CelulleMain originalCell;
    [SerializeField]
    private LinkClass currentLink;
    #endregion

    private void Awake()
    {
        if (cellManagerInstance)
        {
            Destroy(gameObject);
        }
        else
        {
            cellManagerInstance = this;
            DontDestroyOnLoad(gameObject);
        }

        mainCamera = Camera.main;
    }
    private void Update()
    {
        FunctionInputs();
    }
    private void FixedUpdate()
    {
        if (isDragging)
        {
            Drag(Helper.ReturnHit(Input.mousePosition , mainCamera));
        }
    }
    private void FunctionInputs()
    {
        if (!UIManager.uiManagerInstance.inUI)
        {
            if (UnityEngine.Input.GetMouseButtonDown(0) && !creatingAnNewLink)
            {
                Get(Helper.ReturnHit(Input.mousePosition, mainCamera));
            }
            if (isDragging && UnityEngine.Input.GetMouseButtonUp(0))
            {
                Drop();
            }
            if (creatingAnNewLink)
            {
                CreateNewLink(Helper.ReturnHit(Input.mousePosition, mainCamera));
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(1))
            {
                UIManager.uiManagerInstance.DesactivateCellShop();
            }
        }
    }

    #region Fonctions
    // quand le joueur veut placer une infrastructure
    private void Drop()
    {
        //clean rg
        rg.useGravity = true;
        rg.isKinematic = false;
        rg = null;
        //clean col
        col.enabled = true;
        col = null;
        // clean current object 
        checkDrag.hasBeenDropped = true;
        currentObject = null;
        isDragging = false;
    }
    // quand le joueur selectionne une infrastructure
    private void Get(RaycastHit hit)
    {
        currentObject = hit.transform.gameObject;
        checkDrag = hit.transform.GetComponent<IsDragable>();
        originalCell = currentObject.GetComponent<CelulleMain>();
        //check pour blobby ou cell
        if (checkDrag)
        {
            // ser à drag les nouvelles cellules ou le blobby King
            if (!checkDrag.hasBeenDropped || checkDrag.isKingJelly)
            {
                isDragging = true;
                //reference et modifie col
                col = currentObject.GetComponent<Collider>();
                col.enabled = false;
                //reference et modifie rg
                rg = currentObject.GetComponent<Rigidbody>();
                rg.useGravity = false;
            }
            //permet de crée les liens
            else if (!originalCell.noMoreLink)
            {
                if (originalCell.myCellTemplate.cellsEnableToBuild.Length <= 0)
                {
                    currentObject = null;
                    checkDrag = null;
                    originalCell = null;
                    Debug.Log("Ce batiment n'as pas d'output ");
                    return;
                }
                creatingAnNewLink = true;
                GameObject newLine = new GameObject("link");

                //Ref / orga
                currentLine = newLine.AddComponent<LineRenderer>();
                currentLine.transform.SetParent(originalCell.transform, true);
                currentLink = currentLine.gameObject.AddComponent<LinkClass>();
                //Tweaking
                currentLine.startWidth = 0.2f;
                currentLine.material = new Material(Shader.Find("Sprites/Default"));
                //Setup
                currentLine.positionCount = 2;
                startPosLine = currentObject.transform.position;
                currentLine.SetPosition(0, startPosLine);
            }
        }
    }
    // quand le joueur a une infrastructure sélectionnée pour la première fois
    private void Drag(RaycastHit hit)
    {
        Vector3 pos = hit.point;
        rg.MovePosition(Vector3.SmoothDamp(rg.position, pos, ref refSmooth, delay));
    }



    // ça va servir à faire du pooling par la suite , pour l'instant stocke les link mort dans la pool  
    private void SupressCurrentLine()
    {
        currentLine.gameObject.SetActive(false);
        currentLine.gameObject.transform.SetParent(linkPool.transform);
        originalCell = null;
        currentLine = null;
        creatingAnNewLink = false;
    }
    public  void NewCellFromMenu(GameObject cellfromMenu , IsDragable dragable)
    {
        currentLine.SetPosition(1, cellfromMenu.transform.position);
        dragable.hasBeenDropped = true;
        ReferencingTheNewCell(cellfromMenu.GetComponent<CelulleMain>());
        if (creatingAnNewLink)
        {
            SupressCurrentLine();
        }
        UIManager.uiManagerInstance.DesactivateCellShop();

    }
    //Gére Le com entre input et Cellule 
    private void ReferencingTheNewCell(CelulleMain linkedCell)
    {
        // si il y a trop de lien 
        if (linkedCell.noMoreLink == true)
        {
            Debug.Log("suce pute ");
            SupressCurrentLine();
            return;
        }
        currentLine.SetPosition(1, linkedCell.transform.position);
        currentLine.startColor = Color.green;
        currentLine.endColor = Color.green;

        //originalCell.outputCell.Add(linkedCell);
        originalCell.AddLink(currentLink, true);
        linkedCell.AddLink(currentLink, false);

        //c'est du debug
        creatingAnNewLink = false;
        currentLine = null;
        currentObject = null;
    }
    private void CreateNewLink(RaycastHit hit)
    {
        // Permet de draw la line en runtime 
        float distance = Vector3.Distance(startPosLine, hit.point);
        if (distance <= originalCell.myCellTemplate.range / 2)
        {
            endPosLine = new Vector3(hit.point.x, startPosLine.y, hit.point.z);
            currentLine.SetPosition(1, endPosLine);
        }
        else
        {
            Vector3 direction = (hit.point - startPosLine);
            direction = new Vector3(direction.x, 0, direction.z);
            direction = direction.normalized;
            endPosLine = startPosLine + direction * originalCell.myCellTemplate.range / 2;
            //prout
            currentLine.SetPosition(1, endPosLine);
        }


        if (Input.GetMouseButtonUp(0))
        {
            RaycastHit hit2;
            Physics.Raycast(transform.position, endPosLine - transform.position, out hit2, 10000f);
            CelulleMain linkedCell = hit2.transform.gameObject.GetComponent<CelulleMain>();


            if (linkedCell != null && linkedCell != this)
            {
                ReferencingTheNewCell(linkedCell);

            }
            // si c'est dans le vide , un peu sale faudra check  avec le collider ou une range 
            //permet d'ouvrir le menu de création de cellule
            else
            {
                UIManager.uiManagerInstance.InUiSelection(endPosLine , originalCell , currentLine);
            }
        }


    }

    #endregion
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;

[RequireComponent(typeof(MeshCollider))]  //typeof(MeshRenderer), typeof(MeshFilter),
public class CelulleMain : MonoBehaviour
{
    #region Variables
    [Tooltip("glissé l'un des srcyptable object structure ici")]
    public CelluleTemplate myCellTemplate;


    // public List<CelulleMain> outputCell;
    [Header("REF")]
    public TextMeshPro NBlob;
    public TextMeshPro NLink;
    public MeshFilter mF;
    public MeshRenderer mR;
    [Header("Debug")]
    public List<LinkClass> links = new List<LinkClass>();
    public bool noMoreLink;
    public int BlobNumber;

    private List<LinkClass> outputLinks = new List<LinkClass>();
    private SphereCollider rangeCollider;
    private MeshCollider mC;
    private int currentIndex;
    private bool isDead = false;
    #endregion

    public virtual void Awake()
    {
        //Pour le delegate qui gére le tic k
        TickManager.doTick += BlobsTick;
        rangeCollider = transform.gameObject.AddComponent<SphereCollider>();
        rangeCollider.radius = myCellTemplate.range / 2;
        // pour Pas qu'il y est 1000 colllider
        rangeCollider.enabled = false;


        // A changer en mettant les graphs sur un nouveau gameobject en enfant 
        //mR = transform.GetComponent<MeshRenderer>();
        //mF = transform.GetComponent<MeshFilter>();
        mC = transform.GetComponent<MeshCollider>();

        mR.material = myCellTemplate.mat;
        mF.mesh = myCellTemplate.mesh;
        mC.sharedMesh = myCellTemplate.mesh;


        mC.convex = true;
        //UI init 
        NBlob.text = (BlobNumber + " / " + myCellTemplate.storageCapability);
        NLink.text = (links.Count + " / " + myCellTemplate.linkCapability);

    }


    #region Interface 
    public virtual void Died()
    {
        isDead = true;
        TickManager.doTick -= BlobsTick;
        int I = links.Count;
        for (int i = 0; i < I; i++)
        {
            if (i >= I)
            {
                return;
            }
            //Retire les links de haut en bas 
            links[I - i - 1].Break();
        }
        for (int i = 0; i < BlobNumber; i++)
        {
            //Debug.Log("SI TU VOIS ÇA C'EST QUE LES BLOB SONT ENCORE INSTANCIE EN SALE AINSI QUE LEUR RIGIDBODY ");
            //GameObject blob = Instantiate(myCellTemplate.blopPrefab, transform.position, Quaternion.identity);
            //Rigidbody rb = blob.GetComponent<Rigidbody>();
            //Vector3 dir = new Vector3(Random.Range(-1, 1), Random.Range(0.1f, 1f), Random.Range(-1, 1)) * myCellTemplate.impulseForce_Death;
            //rb.AddForce(dir, ForceMode.Impulse);
            //blob.GetComponent<Blob>().blobType = BlobBehaviour.BlobType.mad;
        }
        transform.gameObject.SetActive(false);
        return;
    }
    public virtual void BlobsTick()
    {
        AddBlob(myCellTemplate.prodPerTick);

        //ça marche bien mais à voir si quand 1 batiment meure la produciton saute avec ou pas
        for (int i = 0; i < myCellTemplate.rejectPower_RF; i++)
        {
            if (BlobNumber > 0 && outputLinks.Count > 0)
            {
                if (currentIndex >= outputLinks.Count)
                {
                    return;
                }
                outputLinks[currentIndex].Transmitt();
                currentIndex++;
                currentIndex = Helper.LoopIndex(currentIndex, outputLinks.Count);
            }
        }
        NBlob.text = (BlobNumber + " / " + myCellTemplate.storageCapability);

    }
    public virtual void AddLink(LinkClass linkToAdd, bool output)
    {

        links.Add(linkToAdd);
        NLink.text = (links.Count + " / " + myCellTemplate.linkCapability);

        if (output)
        {
            linkToAdd.Init(this);
            outputLinks.Add(linkToAdd);
            SortingLink();
        }
        else
        {
            linkToAdd.inputCell = this;
        }

        if (links.Count >= myCellTemplate.linkCapability)
        {
            noMoreLink = true;
        }

    }
    public virtual void RemoveLink(LinkClass linkToRemove)
    {
        if (links.Count < myCellTemplate.linkCapability)
        {
            noMoreLink = false;
        }
        outputLinks.Remove(linkToRemove);
        links.Remove(linkToRemove);

        NLink.text = (links.Count + " / " + myCellTemplate.linkCapability);
    }
    public virtual void AddBlob(int Amount)
    {
        BlobNumber += Amount;
        //UI update
       // NBlob.text = (BlobNumber + " / " + myCellTemplate.storageCapability);
        if (BlobNumber > myCellTemplate.storageCapability && !isDead)
        {
            Died();
        }
    }
    public virtual void RemoveBlob(int Amount)
    {
        BlobNumber -= Amount;
        //UI update
        NBlob.text = (BlobNumber + " / " + myCellTemplate.storageCapability);
    }

    //tri les output 
    public virtual void SortingLink()
    {
        outputLinks = outputLinks.OrderBy(t => t.angle).ToList();
    }
    #endregion
}

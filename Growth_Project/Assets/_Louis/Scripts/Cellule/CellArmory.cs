﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellArmory : CelulleMain
{
    [Header("SPECIFICITE CELL")]
    public GameObject targetDirection;
    public int tickForActivation;

    private int currentTick;

    public override void BlobsTick()
    {
        currentTick++;
        if (currentTick == tickForActivation)
        {
            currentTick = 0;

            if (BlobNumber > 0)
            {
                for (int i = 0; i < myCellTemplate.rejectPower_RF; i++)
                {
                    BlobNumber--;
                    Instantiate(myCellTemplate.blopPrefab, targetDirection.transform.position, Quaternion.identity);
                }
            }
        }
        NBlob.text = (BlobNumber + " / " + myCellTemplate.storageCapability);
    }
}
